from django.contrib import admin
from .models import Product, Photo, Category


class PhotosInline(admin.TabularInline):
    fk_name = 'product'
    model = Photo


class ProductAdmin(admin.ModelAdmin):
    inlines = [PhotosInline, ]


admin.site.register(Product, ProductAdmin)
admin.site.register(Category)
