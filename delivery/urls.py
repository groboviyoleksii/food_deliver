from django.contrib import admin
from django.urls import path, include

from delivery.views import HomeView, ProductDetailView
urlpatterns = [
    path('', HomeView.as_view(), name='home'),
    path('detail/<str:slug>', ProductDetailView.as_view(), name='detail'),
]

