from django.shortcuts import render

# Create your views here.
from django.views.generic import TemplateView, ListView, DetailView

from cart.models import Order
from delivery.models import Product, Category


class HomeView(ListView):
    model = Product
    template_name = '../templates/delivery/home.html'
    context_object_name = 'products'
    paginate_by = 1

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(**kwargs)
        context['category'] = Category.objects.all()
        return context

    def get_queryset(self):
        qs = self.model.objects.all()
        if self.request.GET.get('category'):
            print(self.request.GET['category'])
            if self.request.GET['category'] == 'all':
                return qs
            return qs.filter(category__name=self.request.GET['category'])
        return qs


class ProductDetailView(DetailView):
    model = Product
    context_object_name = 'product'
    template_name = '../templates/delivery/detail.html'
    slug_field = 'slug'
