from django.db import models
from django.urls import reverse
from django_extensions.db.fields import AutoSlugField


class Category(models.Model):
    name = models.CharField('category name', max_length=255, null=True)

    def __str__(self):
        return self.name


class Product(models.Model):
    description = models.TextField('description', null=True)
    name = models.CharField('product name', max_length=255, null=True)
    product_composition = models.TextField('product composition', null=True)
    price = models.DecimalField('price', decimal_places=2, max_digits=10)
    main_photo = models.ImageField(upload_to='photo/product', null=True)
    category = models.ManyToManyField(Category)
    slug = AutoSlugField(max_length=50, unique=True, populate_from='name')
    discount_price = models.FloatField('discount price', blank=True, null=True)


    def get_absolute_url(self):
        return reverse('detail', kwargs={'slug': self.slug})

    def __str__(self):
        return self.name


class Photo(models.Model):
    photo = models.ImageField(upload_to='photo/product')
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='photo')
