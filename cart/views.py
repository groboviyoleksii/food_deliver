from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404, redirect
from django.urls import reverse_lazy
from django.utils import timezone
from django.views.generic.base import View

from authorization_system.models import Customer
from cart.forms import CheckoutForm
from cart.models import OrderItem, Order, Payment
from delivery.models import Product

import stripe
from stripe import error

from food_delivery.settings import STRIPE_SECRET_KEY

stripe.api_key = STRIPE_SECRET_KEY


class OrderSummaryView(LoginRequiredMixin, View):
    def get(self, *args, **kwargs):
        try:
            order = Order.objects.get(user=self.request.user, ordered=False)
            context = {
                'object': order
            }
            return render(self.request, 'cart/cart_page.html', context)
        except ObjectDoesNotExist:
            messages.warning(self.request, "You do not have an active order")
            return redirect("/")

    def get_login_url(self):
        return reverse_lazy('login')


class StripePaymentView(View):
    def get(self, *args, **kwargs):

        return render(self.request, 'cart/payment.html')

    def post(self, *args, **kwargs):
        token = self.request.POST.get('stripeToken')
        order = Order.objects.get(user=self.request.user, ordered=False)
        amount = int(order.get_total() * 100)
        user = Customer.objects.get(email=self.request.user.email)

        try:
            charge = stripe.Charge.create(
                amount=amount,
                currency="usd",
                source="tok_visa",
            )

            payment = Payment()
            payment.stripe_charge_id = charge['id']
            payment.user = self.request.user
            payment.amount = order.get_total()
            payment.save()

            order.ordered = True
            order.payment = payment
            order.save()

            user.bonuses += order.get_total() / 100
            user.save()

            messages.success(self.request, 'Your order was successful')
            return redirect('/')
        except stripe.error.CardError as e:
            print('Status is: %s' % e.http_status)
            print('Code is: %s' % e.code)
            print('Param is: %s' % e.param)
            print('Message is: %s' % e.user_message)
            return redirect('/')
        except stripe.error.RateLimitError as e:
            messages.error(self.request, 'Rate limit error')
            return redirect('/')
        except stripe.error.InvalidRequestError as e:
            messages.error(self.request, 'Invalid parameters')
            return redirect('/')
        except stripe.error.AuthenticationError as e:
            messages.error(self.request, 'Not authenticated')
            return redirect('/')
        except stripe.error.APIConnectionError as e:

            messages.error(self.request, 'Network error')
            return redirect('/')
        except stripe.error.StripeError as e:
            messages.error(self.request, 'Something wrong')
            return redirect('/')
        except Exception as e:
            messages.error(self.request, 'Serious error')
            return redirect('/')


@login_required(login_url=reverse_lazy('login'))
def add_to_cart(request, slug):
    item = get_object_or_404(Product, slug=slug)
    order_item, created = OrderItem.objects.get_or_create(
        item=item,
        user=request.user,
        ordered=False
    )
    order_qs = Order.objects.filter(user=request.user, ordered=False)
    if order_qs.exists():
        order = order_qs[0]
        if order.items.filter(item__slug=item.slug).exists():
            order_item.quantity += 1
            order_item.save()
            messages.info(request, "This item quantity was updated.")
            return redirect(request.META.get('HTTP_REFERER'))
        else:
            order.items.add(order_item)
            messages.info(request, "This item was added to your cart.")
            return redirect("home")
    else:
        ordered_date = timezone.now()
        order = Order.objects.create(
            user=request.user, ordered_date=ordered_date)
        order.items.add(order_item)
        messages.info(request, "This item was added to your cart.")
        return redirect("home")


def remove_from_cart(request, slug):
    item = get_object_or_404(Product, slug=slug)
    order_qs = Order.objects.filter(
        user=request.user,
        ordered=False
    )
    if order_qs.exists():
        order = order_qs[0]
        if order.items.filter(item__slug=item.slug).exists():
            order_item = OrderItem.objects.filter(
                item=item,
                user=request.user,
                ordered=False
            )[0]
            order.items.remove(order_item)
            order_item.delete()
            messages.info(request, "This item was removed from your cart.")
            return redirect('cart_page')
        else:
            messages.info(request, "This item was not in your cart")
            return redirect("home", slug=slug)
    else:
        messages.info(request, "You do not have an active order")
        return redirect("home", slug=slug)


@login_required
def remove_single_item_from_cart(request, slug):
    item = get_object_or_404(Product, slug=slug)
    order_qs = Order.objects.filter(
        user=request.user,
        ordered=False
    )
    if order_qs.exists():
        order = order_qs[0]
        if order.items.filter(item__slug=item.slug).exists():
            order_item = OrderItem.objects.filter(
                item=item,
                user=request.user,
                ordered=False
            )[0]
            if order_item.quantity > 1:
                order_item.quantity -= 1
                order_item.save()
            else:
                order.items.remove(order_item)
            messages.info(request, "This item quantity was updated.")
            return redirect("cart_page")
        else:
            messages.info(request, "This item was not in your cart")
            return redirect("detail", slug=slug)
    else:
        messages.info(request, "You do not have an active order")
        return redirect("detail", slug=slug)


class CheckoutView(View):
    def get(self, request, *args, **kwargs):
        form = CheckoutForm()
        try:
            order = Order.objects.get(user=self.request.user, ordered=False)
            context = {
                'object': order,
                'form': form,

            }
            return render(self.request, 'cart/checkout.html', context)
        except ObjectDoesNotExist:
            messages.warning(self.request, "You do not have an active order")
            return redirect("/")

    def post(self, *args, **kwargs):
        payment = Payment()
        order = Order()
        user = Customer()
        form = CheckoutForm(self.request.POST or None)
        try:
            if form.is_valid():
                payment_option = form.cleaned_data.get('payment_options')
                if payment_option == 'S':
                    return redirect('stripe')
                elif payment_option == 'B':
                    try:
                        order.ordered = True
                        order.payment = payment
                        order.save()
                        payment.id = order.id
                        payment.user = self.request.user
                        payment.amount = order.get_total()
                        payment.save()
                        user.bonuses -= order.get_total()
                        user.save()
                        messages.success(self.request, 'Your order was successful')
                        return redirect('/')

                    except Exception as Not_enough_bonuses:
                        messages.error(self.request, 'Not enough bonuses')
                        return redirect('/')
                elif payment_option == 'C':
                    order.ordered = True
                    order.payment = payment
                    order.save()
                    payment.id = order.id
                    payment.user = self.request.user
                    payment.amount = order.get_total()
                    payment.save()
                    messages.success(self.request, 'Your order was successful')
                    return redirect('/')
                else:
                    messages.warning(
                        self.request, "Invalid payment option selected")
                    return redirect('checkout')
        except ObjectDoesNotExist:
            messages.warning(self.request, 'Form is not valid')
