from django.contrib import admin
from django.urls import path, include

from cart.views import OrderSummaryView, add_to_cart, remove_from_cart, remove_single_item_from_cart, CheckoutView, \
    StripePaymentView

urlpatterns = [
    path('cart', OrderSummaryView.as_view(), name='cart_page'),
    path('add-to-cart/<slug>', add_to_cart, name='add_to_cart'),
    path('remove-from-cart/<slug>', remove_from_cart, name='remove_from_cart'),
    path('remove-single-item/<slug>', remove_single_item_from_cart, name='remove_single_item'),
    path('checkout', CheckoutView.as_view(), name='checkout'),
    path('stripe/', StripePaymentView.as_view(), name='stripe'),
]
