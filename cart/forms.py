from django import forms

PAYMENT_CHOICE = (
    ('S', 'stripe'),
    ('B', 'bonuses'),
    ('С', 'cash')
)


class CheckoutForm(forms.Form):
    name = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Ivan'}))
    street_address = forms.CharField(widget=forms.TextInput(attrs={
        'placeholder': '1234 Main St'
    }))
    number = forms.CharField(widget=forms.TextInput(attrs={
        'placeholder': '3801488228'
    }))
    description = forms.CharField(widget=forms.Textarea(attrs={'placeholder': 'Some notes'}))
    payment_options = forms.ChoiceField(widget=forms.RadioSelect, choices=PAYMENT_CHOICE)
