from django import forms
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from .models import Customer


class AuthUserForm(AuthenticationForm):
    class Meta:
        fields = ('password', 'email')


class RegisterUserForm(UserCreationForm):
    class Meta:
        model = Customer
        fields = ('email', 'password1', 'password2')


class CustomerForm(forms.ModelForm):
    class Meta:
        model = Customer
        fields = ('first_name', 'last_name', 'street_address', 'apartments_address', 'number', 'about', 'photo')
