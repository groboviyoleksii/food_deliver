from django.conf import settings
from django.contrib.auth import authenticate, login
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import LoginView, LogoutView
from django.urls import reverse_lazy
from django.views.generic import CreateView, UpdateView, DetailView

from .forms import AuthUserForm, RegisterUserForm, CustomerForm
from .models import Customer
from cart.models import Order


class AuthView(LoginView):
    template_name = 'authorization_system/login.html'
    form_class = AuthUserForm
    success_url = reverse_lazy('home')

    def get_success_url(self):
        return self.success_url


class RegisterUserView(CreateView):
    model = Customer
    template_name = 'authorization_system/register.html'
    form_class = RegisterUserForm
    success_url = reverse_lazy('home')

    def form_valid(self, form):
        form_valid = super().form_valid(form)
        email = form.cleaned_data["email"]
        password = form.cleaned_data["password1"]
        auth_user = authenticate(email=email, password=password)
        login(self.request, auth_user)
        return form_valid


class ProfileView(LoginRequiredMixin, UpdateView):
    login_url = '/login'
    slug_field = 'slug'
    model = Customer
    template_name = 'authorization_system/profile.html'
    context_object_name = 'user'
    form_class = CustomerForm


class LogoutUserView(LogoutView):
    next_page = reverse_lazy('home')
