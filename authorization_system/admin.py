from django.contrib import admin

from authorization_system.models import Customer

admin.site.register(Customer)
