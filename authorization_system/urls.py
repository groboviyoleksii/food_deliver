
from django.urls import path, include

from authorization_system import views
from delivery.views import HomeView

urlpatterns = [
    path('login/', views.AuthView.as_view(), name='login'),
    path('register/', views.RegisterUserView.as_view(), name='register'),
    path('logout/', views.LogoutUserView.as_view(), name='logout'),
    path('profile/<slug>', views.ProfileView.as_view(), name='profile'),
]
