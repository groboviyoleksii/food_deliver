from django.apps import AppConfig


class AuthorizationSystemConfig(AppConfig):
    name = 'authorization_system'
