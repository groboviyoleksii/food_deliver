from django.conf import settings
from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.contrib.auth.models import PermissionsMixin
from django.db import models
from django.urls import reverse
from django.utils import timezone
from django.utils.text import slugify
from django_extensions.db.fields import AutoSlugField


class CustomUserManager(BaseUserManager):
    def create_user(self, email, password, **extra_fields):
        if not email:
            raise ValueError('The Email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, username, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_active', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')
        return self.create_user(username, password, **extra_fields)


class Customer(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField('email address', unique=True)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    date_joined = models.DateTimeField(default=timezone.now)
    username = models.CharField('username', max_length=255, null=True, blank=True)
    street_address = models.CharField('street_address', max_length=255, null=True, blank=True)
    apartments_address = models.CharField('apartments_address', max_length=255, null=True, blank=True)
    number = models.CharField('phone number', max_length=255, null=True, blank=True)
    first_name = models.CharField('first name', max_length=255, null=True, blank=True, default='')
    last_name = models.CharField('last name', max_length=255, null=True, blank=True)
    photo = models.ImageField(upload_to='photo/profile_photo', null=True)
    about = models.TextField('about', null=True, blank=True)
    bonuses = models.DecimalField('bonuses', decimal_places=2, max_digits=5, null=True, default=0)
    slug = AutoSlugField(max_length=50, unique=True, populate_from='email')

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = CustomUserManager()

    def get_absolute_url(self):
        return reverse('profile', kwargs={'slug': self.slug})

    def __str__(self):
        return f'{self.first_name or self.email}'
